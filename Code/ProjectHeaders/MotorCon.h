/****************************************************************************

  Header file for MotorCon
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef MotorCon_H
#define MotorCon_H

#include <stdint.h>
#include <stdbool.h>
#include "ES_Configure.h"   
#include "ES_Types.h"     
#include "ES_Events.h"
#include "ES_Port.h"                // needed for definition of REENTRANT

// typedefs for the states
typedef enum
{
  InitState, Running
}MotorState_t;

// Public Function Prototypes
bool InitMotorCon(uint8_t Priority);
bool PostMotorCon(ES_Event_t ThisEvent);
ES_Event_t RunMotorCon(ES_Event_t ThisEvent);


#endif /* ServTemplate_H */

