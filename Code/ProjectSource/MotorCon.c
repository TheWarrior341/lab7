    /****************************************************************************
 Module
   MotorCon.c

 Revision
   1.0.1

 Description
   This is the service for controlling the speed of a motor.

 Notes

****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
// This module
#include "../ProjectHeaders/MotorCon.h"

// debugging printf()
//#include "dbprintf.h"

// Hardware
#include <xc.h>
#include <proc/p32mx170f256b.h>
#include <sys/attribs.h> // for ISR macors

// Event & Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
#include "ES_Port.h"
#include "ES_Types.h"     
#include "ES_Events.h"

//ADC module
#include "PIC32_AD_Lib.h"
/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static void InitTMR4(void);
static void InitTMR2(void);
static void InitTMR3(void);
static void InitOC(void);
static void InitIC4(void);
/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
//State variable for this State machine
MotorState_t CurrentState;
static volatile uint16_t PulseWidth, LastTime, CurrentTime;
static volatile float SumError = 0;
static float RPM = 0;
static float Kp = .6;
static float Ki = 0.03;
static float TargetRPM = 0;
static volatile float RequestedDuty = 0;
static volatile float RPMError = 0  ;

/*------------------------------ Module Code -  -----------------------------*/
/****************************************************************************
 Function
     InitInputCaptureService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
 *  Saves away the priority
 *  Initializes all required variables for this service
 
 * 
****************************************************************************/
bool InitMotorCon(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  LastTime = 0;
  PulseWidth = 0;
  CurrentTime = 0;
  //Initialize the ADC module for RB12
  ADC_ConfigAutoScan(BIT12HI,1);
  
   //Set CurrentState to Init 
   CurrentState = InitState;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostMotorCon

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

****************************************************************************/
bool PostMotorCon(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunInputCaptureService

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
 * The main service for calibrating dot length, and calculating average morse
 * element lengths
 * Receives high and low pulse events, bad pulse and bad space events, 
 * calibration complete event, and calculate average events
 * 
 * Implements a nested switch based on the state of this state machine

****************************************************************************/
ES_Event_t RunMotorCon(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  switch(CurrentState) //Start a nested switch on CurrentState
  {
      case InitState:
      {
                if(ThisEvent.EventType == ES_INIT) //Respond to Init event
                {
                    //Call the functions for initializing the timers and systems                    
                    InitTMR2();
                    InitOC();
                    InitTMR3();
                    InitIC4();
                    InitTMR4();
                    printf("Init successful\n\r");
                    //Initialize a framework timer for 0.1 seconds.
                    ES_Timer_InitTimer(POT_TIMER,100);
                    CurrentState = Running;  
                 }
                
      } //End Init state
      break;
      
      case Running:
      {
          if(ThisEvent.EventType == ES_TIMEOUT)
          {
              
              //Reinit the timer for 0.1 seconds
              ES_Timer_InitTimer(POT_TIMER,100);
              //Read the value on the potentiometer
              uint32_t pot_val[1];
              ADC_MultiRead(pot_val);
              //printf("Pot val = %d\n\r",pot_val[0]);
              //Calculate the corresponding target speed of motor
              TargetRPM = pot_val[0]*60/1024;
              //Find the value needed in OC1RS
              OC1RS = (uint16_t)RequestedDuty*500/100;
              //Calculate the motor speed from the encoder pulse width
              RPM = (float)(78125*60)/(float)(PulseWidth*6*150);    
              if(RPM>100) RPM = 0;
              //Report RPM of motor
              printf(" Current RPM = %f\n\r", RPM); 
              printf(" Target RPM = %f\n\n\r", TargetRPM);
          }
          
      }
      break;
  }
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/* InitTMR4:
Sets up the timer used for evaluating the control law*/
static void InitTMR4(void)
{
    // turn timer off
  T4CONbits.ON = 0;
  // Use internal peripheral clock
  T4CONbits.TCS = 0;
  // setup for 16 bit mode
  T4CONbits.T32 = 0;
  // Set up the prescale value 
  T4CONbits.TCKPS = 0b111;  
  // Set the timer period register (PRy)
  PR4 =  390; 

  IEC0SET = _IEC0_T4IE_MASK;

  //Clear the Timer Flag
  IFS0CLR = _IFS0_T4IF_MASK;

  //Set priority for Timer 
  IPC4bits.T4IP = 6;
  // clear timer
  TMR4 = 0;
  // start timer
  T4CONbits.ON = 1;  
}


/* InitTMR2:
Sets up the timer used for the output compare module for driving the motor*/

static void InitTMR2(void)
{
  // turn timer off
  T2CONbits.ON = 0;
  // Use internal peripheral clock
  T2CONbits.TCS = 0;
  // setup for 16 bit mode
  T2CONbits.T32 = 0;
  // Set up the prescale value determined based on time constant.
  T2CONbits.TCKPS = 0b011;  
  // Set the PWM period by writing to the selected timer period register (PRy)
  PR2 =  500; 

  IEC0SET = _IEC0_T2IE_MASK;

  //Clear the Timer Flag
  IFS0CLR = _IFS0_T2IF_MASK;

  //Set priority for Timer 
  IPC2bits.T2IP = 6;
  // clear timer
  TMR2 = 0;
  // start timer
  T2CONbits.ON = 1;  
 
}


/*Output Compare Initialization Routine
Parameters: None; Returns: Nothing
Sets up the output compare system for producing PWM signals for running the motor. Also
configures interrupts in general.*/

static void InitOC(void)
{
    //Set up for multiple interrupt vectors
    INTCONbits.MVEC = 1;
    //Enable interrupts globally
    __builtin_enable_interrupts(); 
    //Declare pin A0 to be a digital output
    ANSELA = 0;
    TRISAbits.TRISA0 = 0;
    //Declare pin A1 as output and set it low
    TRISAbits.TRISA1 = 0;
    LATAbits.LATA1 = 0;
    //Map the desired pin(A0) to be an Output compare line     
    RPA0R =  0b0101;
    //Disable OCM bit before starting to make changes 
    OC1CONbits.ON = 0;
    //Clear OCM bit
    OC1CONbits.OCM = 0b000;
    //Set it to continue module operation during idling
    OC1CONbits.SIDL = 0;
    //Set OC32 bit to 0 as we have a 16-bit timer source
    OC1CONbits.OC32 = 0;
    //Set OCTSEL bit to 0 so that we use Timer2 as the clock source
    OC1CONbits.OCTSEL = 0;
    //Set OCMbits to 110: PWM mode on OC1, Fault pin disable
    OC1CONbits.OCM = 0b110;
    //Write an initial value of 0 into the OC1R and OC1RS registers.
    OC1R = 100; OC1RS = 100;
    OC1CONbits.ON = 1;
    
}
/* InitTMR3:
Sets up the timer used for the input capture module for reading the encoder signal*/

static void InitTMR3(void)
{
  // turn timer off
  T3CONbits.ON = 0;
  // Use internal peripheral clock
  T3CONbits.TCS = 0;
  // Set up the prescale value determined based on encoder signal.
  T3CONbits.TCKPS = 0b111;  
  // Set the timer period register (PRy)
  PR3 =  0xFFFF; 

  IEC0SET = _IEC0_T3IE_MASK;

  //Clear the Timer Flag
  IFS0CLR = _IFS0_T3IF_MASK;


  IPC3bits.T3IP = 6;
  // clear timer
  TMR3 = 0;
  //Start the timer
  T3CONbits.ON = 1;
}

/*Input Capture Initialization Routine
Parameters: None; Returns: Nothing
Sets up the input capture module to read the encoder channel*/

static void InitIC4(void)
{
  //Make pin RB4 a digital input
  ANSELBbits.ANSB15 = 0;
  TRISBbits.TRISB15 = 1;
  //Map the pin to the input capture
  IC4R = 0b0011;
  //Enable input capture
  IC4CONbits.ON=0;
  //Operate in idle mode
  IC4CONbits.SIDL = 0;
  //Set it to capture rising edge first
  IC4CONbits.FEDGE = 1;
  //Set it to be a 16-bit timer resource capture
  IC4CONbits.C32 = 0;
  //Set Timer 3 as the source
  IC4CONbits.ICTMR = 0;
  //Set input capture to interrupt on every capture event
  IC4CONbits.ICI = 0b00;
  //Configure it to capture every rising edge
  IC4CONbits.ICM = 0b011;
  //Enabling the local input capture interrupt
  IEC0bits.IC4IE = 1;
  //Clear Interrupt Flag
  IFS0CLR = _IEC0_IC4IE_MASK;
  // set priority for the input capture interrupt
  IPC4bits.IC4IP = 7;    
  //Starting input capture
  IC4CONbits.ON = 1;
  
}

void __ISR(_TIMER_2_VECTOR, IPL6SOFT) Timer2ISR(void)
{
  //Clear the Timer Flag
  IFS0CLR = _IFS0_T2IF_MASK;
}
/*Timer3ISR: 
 * The interrupt service routine for the IC timer. Clears the interrupt flag
 * Parameters: None
 * Returns: void 
 */
void __ISR(_TIMER_3_VECTOR, IPL6SOFT) Timer3ISR(void)
{
  //Clear the Timer Flag
  IFS0CLR = _IFS0_T3IF_MASK;
}

/*InputCaptureISR: 
 * The interrupt service routine for the input capture system
 * Reads the IC buffer, and calculates  the pulse width
 * 
 * Parameters: None
 * Returns: void 
 */
void __ISR(_INPUT_CAPTURE_4_VECTOR, IPL7SOFT) InputCaptureISR(void)
{
    //Read IC4BUF and store value
    CurrentTime = (uint16_t)IC4BUF;
    // clear capture interrupt flag
    IFS0CLR = _IFS0_IC4IF_MASK;
    if(IFS0bits.T3IF == 1 && CurrentTime<0x8000)
    {
        //
        IFS0CLR = _IFS0_T3IF_MASK;
    }
    //Calculate the pulsewidth as CurrentTime minus LastTime
    PulseWidth = CurrentTime - LastTime;
    //Update the value of LastTime    
    LastTime = CurrentTime;
    return;
}
/*Timer3ISR: 
 * The interrupt service routine for the Control law timer. 
 * Clears the interrupt flag, and calculates the control law required for PI control
 * Parameters: None
 * Returns: void 
 */
void __ISR(_TIMER_4_VECTOR, IPL6SOFT) Timer4ISR(void)
{
    
    //Clear the Timer Flag
    IFS0CLR = _IFS0_T4IF_MASK;
    
    // To allow timing this routine raise a line on entry
    LATBSET = _TRISB_TRISB4_POSITION;
    RPMError = TargetRPM - RPM;
    SumError += RPMError;
    RequestedDuty = Kp*RPMError + Ki*SumError;
    if (RequestedDuty > 100) 
    {
        RequestedDuty = 100;
        SumError-=RPMError;   /* anti-windup */
    }
    else if (RequestedDuty < 0)
    {
        RequestedDuty = 0;
        SumError-=RPMError;   /* anti-windup */
    }
    
    LATBCLR = _TRISB_TRISB4_POSITION; /* end of execution time */
}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

